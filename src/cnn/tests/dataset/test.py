import glob

from os import path
from cnn.dataset.LowResolutionDataset import LowResolutionDataset
from cnn.io.Viewer import Viewer

def test():
	dataset = LowResolutionDataset(scale=8, batch_size=32)
	dataset_path = path.dirname(path.abspath(__file__)) + "/../../../../data/high-resolution/*.jpg"
	files = glob.glob(dataset_path)
	print(dataset_path)
	print("Number of files: {}".format(len(files)))
	loader = dataset.create_batch_loader(files)
	print(loader)
	batch = list(loader.take(1).as_numpy_iterator())[0]
	print(type(batch))
	print(type(batch[0]))

if __name__ == "__main__":
	test()
