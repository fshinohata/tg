from cnn.io.ImageLoader import ImageLoader
from cnn.io.Viewer import Viewer
from os import path

def test():
	loader = ImageLoader()
	viewer = Viewer()

	viewer.show_tensor(loader.load_image_as_tensor(path.dirname(path.abspath(__file__)) + "/test_image.jpg"))

if __name__ == "__main__":
	test()
