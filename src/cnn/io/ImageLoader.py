import tensorflow as tf

class ImageLoader:
	def load_image_as_tensor(self, path: str) -> tf.Tensor:
		image = tf.io.read_file(path)
		image = tf.image.decode_jpeg(image, channels=3)
		image = tf.image.convert_image_dtype(image, tf.float32)
		return image
