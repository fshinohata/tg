import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

class Viewer:
	def show_tensor(self, tensor: tf.Tensor) -> None:
		image = np.squeeze(np.clip(tensor, a_min=0, a_max=1))
		plt.imshow(image)
		plt.show()

	def show_tensors(self, tensors: list[tf.Tensor], labels: list[str]) -> None:
		# plt.figure()
		# manager = plt.get_current_fig_manager()
		# manager.window.state("zoomed")
		figure, axes = plt.subplots(1, len(tensors))
		# figure.set_size_inches(8, 4)
		figure.canvas.manager.window.state("zoomed")

		for i in range(len(tensors)):
			image = np.squeeze(np.clip(tensors[i], a_min=0, a_max=1))
			axes[i].imshow(image)
			axes[i].set_title(labels[i])

		plt.show()
