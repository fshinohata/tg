import random
import tensorflow as tf

from typing import Tuple

from tensorflow.python.ops.image_ops_impl import ResizeMethod
from cnn.io.ImageLoader import ImageLoader


class LowResolutionDataset:
	def __init__(self, scale: int = 8, batch_size: int = 8) -> None:
		self.batch_size = batch_size
		self.scale = scale
		self.loader = ImageLoader()
		self.resize_methods = [ResizeMethod.AREA, ResizeMethod.BICUBIC, ResizeMethod.BILINEAR, ResizeMethod.GAUSSIAN, ResizeMethod.LANCZOS3, ResizeMethod.LANCZOS5, ResizeMethod.MITCHELLCUBIC, ResizeMethod.NEAREST_NEIGHBOR]

	def create_batch_loader(self, files: list[str]) -> tf.data.Dataset:
		shuffled_files = random.sample(files, len(files))
		dataset_files = tf.data.Dataset.from_tensor_slices(shuffled_files)
		dataset_files = dataset_files.map(self._load_image, num_parallel_calls=tf.data.experimental.AUTOTUNE)
		dataset_files = dataset_files.batch(self.batch_size).repeat()
		dataset_files = dataset_files.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
		return dataset_files

	def _load_image(self, path: str) -> Tuple[tf.Tensor, tf.Tensor]:
		ground_truth = self.loader.load_image_as_tensor(path)
		ground_truth_shape = tf.shape(ground_truth)
		resize_method = random.choice(self.resize_methods)
		downsampled = tf.image.resize(ground_truth, size=(ground_truth_shape[0] / self.scale, ground_truth_shape[1] / self.scale), method=resize_method)
		downsampled = tf.image.random_jpeg_quality(downsampled, min_jpeg_quality=60, max_jpeg_quality=100)
		downsampled = tf.image.random_brightness(downsampled, max_delta=0.2)
		downsampled = tf.image.random_contrast(downsampled, lower=0.7, upper=1.3)
		return downsampled, ground_truth
