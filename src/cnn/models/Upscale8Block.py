import tensorflow as tf
import tensorflow.keras.layers as layers


class Upscale8Block(layers.Layer):
	def __init__(self, channels: int = 128) -> None:
		super(Upscale8Block, self).__init__()
		self.conv1 = layers.Conv2D(filters=channels * 16, kernel_size=(3, 3), strides=(1, 1), padding="same")
		self.relu1 = layers.ReLU()
		self.conv2 = layers.Conv2D(filters=channels * 4, kernel_size=(3, 3), strides=(1, 1), padding="same")
		self.relu2 = layers.ReLU()

	def call(self, x: tf.Tensor) -> tf.Tensor:
		x = self.conv1(x)
		x = tf.nn.depth_to_space(x, block_size=4)
		x = self.relu1(x)
		x = self.conv2(x)
		x = tf.nn.depth_to_space(x, block_size=2)
		x = self.relu2(x)
		return x

	def get_config(self) -> dict:
		return { "Upscale8Block": Upscale8Block }

	@classmethod
	def from_config(cls, config):
		return cls(**config)
