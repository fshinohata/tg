import tensorflow as tf
import tensorflow.keras.layers as layers

from typing import Any


class ResidualBlock(layers.Layer):
	def __init__(self, layers_num: int = 4, channels: int = 128) -> None:
		super(ResidualBlock, self).__init__()
		self.layers_num = layers_num
		self.convs = []
		self.lrelus = []
		self.batch_norms = []

		for i in range(layers_num):
			self.convs.append(layers.Conv2D(filters=channels, kernel_size=(3, 3), strides=(1, 1), padding="same"))
			self.batch_norms.append(layers.BatchNormalization())
			if i < layers_num - 1:
				self.lrelus.append(layers.LeakyReLU(alpha=0.2))

	def call(self, x: tf.Tensor) -> tf.Tensor:
		x_copy = tf.identity(x)
		for i in range(self.layers_num):
			x = self.convs[i](x)
			x = self.batch_norms[i](x)
			if i < self.layers_num - 1:
				x = self.lrelus[i](x)
		x = x + x_copy
		return x

	def get_custom_objects(self) -> dict[str, Any]:
		return { "ResidualBlock": ResidualBlock }

	def get_config(self) -> dict[str, int]:
		return { "layers_num": self.layers_num }
