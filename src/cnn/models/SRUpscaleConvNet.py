from typing import Any
import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow.keras.layers as layers


class SRUpscaleConvNet(tfk.Model):
	def __init__(self, layers_num: int = 16, image_channels: int = 3, channels: int = 128):
		super(SRUpscaleConvNet, self).__init__()

		self.layers_num = layers_num
		self.noise = layers.GaussianNoise(stddev=0.15)
		self.convs = []
		self.lrelus = []
		self.batch_norms = []

		for i in range(layers_num):
			self.convs.append(layers.Conv2D(filters=channels, kernel_size=(3, 3), strides=(1, 1), padding="same"))
			self.batch_norms.append(layers.BatchNormalization())
			if i < layers_num - 1:
				self.lrelus.append(layers.LeakyReLU(alpha=0.2))

		self.upscale = Upscale8Block(channels=channels)

		self.out = layers.Conv2D(filters=image_channels, kernel_size=(3, 3), strides=(1, 1), padding="same")

	def call(self, x):
		x = self.noise(x)
		for i in range(self.layers_num):
			x = self.convs[i](x)
			x = self.batch_norms[i](x)
			if i < self.layers_num - 1:
				x = self.lrelus[i](x)
		x = self.upscale(x)
		x = self.out(x)
		return x

	def get_custom_objects(self) -> dict[str, Any]:
		return { "SRUpscaleConvNet": SRUpscaleConvNet, "Upscale8Block": Upscale8Block }

	def get_config(self) -> dict[str, int]:
		return { "layers_num": self.layers_num }

	@classmethod
	def from_config(cls, config):
		return cls(**config)


class Upscale8Block(layers.Layer):
	def __init__(self, channels: int = 128) -> None:
		super(Upscale8Block, self).__init__()
		self.conv1 = layers.Conv2D(filters=channels * 16, kernel_size=(3, 3), strides=(1, 1), padding="same")
		self.relu1 = layers.ReLU()
		self.conv2 = layers.Conv2D(filters=channels * 4, kernel_size=(3, 3), strides=(1, 1), padding="same")
		self.relu2 = layers.ReLU()

	def call(self, x: tf.Tensor) -> tf.Tensor:
		x = self.conv1(x)
		x = tf.nn.depth_to_space(x, block_size=4)
		x = self.relu1(x)
		x = self.conv2(x)
		x = tf.nn.depth_to_space(x, block_size=2)
		x = self.relu2(x)
		return x

	def get_config(self) -> dict:
		return {}

	@classmethod
	def from_config(cls, config):
		return cls(**config)