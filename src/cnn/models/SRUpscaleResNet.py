import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow.keras.layers as layers

from cnn.models.ResidualBlock import ResidualBlock
from cnn.models.Upscale8Block import Upscale8Block
from typing import Any


class SRUpscaleResNet(tfk.Model):
	def __init__(self, blocks_num: int = 4, image_channels: int = 3, channels: int = 128):
		super(SRUpscaleResNet, self).__init__()

		self.input_conv = layers.Conv2D(filters=channels, kernel_size=(3, 3), strides=(1, 1), padding="same")
		self.input_batch_norm = layers.BatchNormalization()
		self.input_lrelu = layers.LeakyReLU(alpha=0.2)

		self.blocks_num = blocks_num
		self.blocks = []

		for i in range(blocks_num):
			self.blocks.append(ResidualBlock(layers_num=4, channels=channels))

		self.upscale = Upscale8Block(channels=channels)

		self.output_conv = layers.Conv2D(filters=image_channels, kernel_size=(3, 3), strides=(1, 1), padding="same")

	def call(self, x: tf.Tensor):
		x = self.input_conv(x)
		x = self.input_batch_norm(x)
		x = self.input_lrelu(x)
		x_copy = tf.identity(x)

		for i in range(self.blocks_num):
			x = self.blocks[i](x)

		x = x + x_copy
		x = self.upscale(x)
		x = self.output_conv(x)
		return x

	def get_custom_objects(self) -> dict[str, Any]:
		return { "SRUpscaleResNet": SRUpscaleResNet, "Upscale8Block": Upscale8Block, "ResidualBlock": ResidualBlock }

	def get_config(self) -> dict[str, int]:
		return { "blocks_num": self.blocks_num }

	@classmethod
	def from_config(cls, config):
		return cls(**config)
