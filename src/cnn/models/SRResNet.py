from typing import Any
import tensorflow.keras as tfk
import tensorflow.keras.layers as layers


class SRResNet(tfk.Model):
	def __init__(self, layers_num: int = 16, image_channels: int = 3, channels: int = 128):
		super(SRResNet, self).__init__()

		self.layers_num = layers_num
		self.convs = []
		self.lrelus = []
		self.batch_norms = []

		for i in range(layers_num):
			self.convs.append(layers.Conv2D(filters=channels, kernel_size=(3, 3), strides=(1, 1), padding="same"))
			self.batch_norms.append(layers.BatchNormalization())
			if i < layers_num - 1:
				self.lrelus.append(layers.LeakyReLU(alpha=0.2))

		self.out = layers.Conv2D(filters=image_channels, kernel_size=(3, 3), strides=(1, 1), padding="same")

	def call(self, x):
		x_copy = x
		x_copy = self.convs[0](x_copy)
		x_copy = self.lrelus[0](x_copy)
		for i in range(self.layers_num):
			x = self.convs[i](x)
			x = self.batch_norms[i](x)
			if i < self.layers_num - 1:
				x = self.lrelus[i](x)
		x = x + x_copy
		x = self.out(x)
		return x

	def get_custom_objects(self) -> dict[str, Any]:
		return { "SRResNet": SRResNet }

	def get_config(self) -> dict[str, int]:
		return { "layers_num": self.layers_num }

	@classmethod
	def from_config(cls, config):
		return cls(**config)
