import tensorflow as tf
import tensorflow.keras as tfk
from tensorflow.python.ops.image_ops_impl import ResizeMethod

from cnn.dataset.LowQualityDataset import LowQualityDataset
from cnn.dataset.LowResolutionDataset import LowResolutionDataset
from cnn.io.ImageLoader import ImageLoader
from cnn.io.Viewer import Viewer
from cnn.logging.Logger import Logger
from cnn.models.SRUpscaleConvNet import SRUpscaleConvNet
from cnn.models.SRUpscaleResNet import SRUpscaleResNet
from cnn.models.SRConvNet import SRConvNet
from cnn.models.SRResNet import SRResNet
from cnn.training.Trainer import Trainer
from datetime import datetime
from glob import glob
from os import path


current_dir = path.dirname(path.abspath(__file__))

def train(network: tfk.Model, network_name: str, training_dataset: tf.data.Dataset, validation_dataset: tf.data.Dataset):
	now = datetime.now()
	logger = Logger("{}/logs/log_{}_{}_{}.txt".format(current_dir, now.year, now.month, now.day))

	trainer = Trainer(name=network_name, network=network, logger=logger, learning_rate=0.004)
	trainer.compile_network()
	trainer.fit(
		training_dataset=training_dataset,
		validation_dataset=validation_dataset,
		epochs=20,
		steps_per_epoch=300,
		iterations=5
	)

def evaluate_plate(network: tfk.Model, plate: str, resize_lr: bool = False) -> None:
	loader = ImageLoader()
	lr_image = loader.load_image_as_tensor("{}/../../data/validation/low-resolution/{}.jpg".format(current_dir, plate))
	hr_image = loader.load_image_as_tensor("{}/../../data/validation/high-resolution/{}.jpg".format(current_dir, plate))
	if resize_lr:
		lr_image = tf.image.resize(lr_image, size=(hr_image.shape[0], hr_image.shape[1]))
	lr_image = tf.expand_dims(lr_image, axis=0)
	lr_prediction = network.predict(lr_image)
	Viewer().show_tensors([lr_image[0], lr_prediction[0], hr_image], ["Input", "Prediction", "Ground Truth"])

def evaluate_akp(network: tfk.Model, resize_lr: bool = False):
	loader = ImageLoader()
	hr_akp_image = loader.load_image_as_tensor("{}/../bicubic/akp-4426.jpg".format(current_dir))
	hr_akp_image = tf.image.resize(hr_akp_image, size=(64, 200), method=ResizeMethod.BICUBIC)
	lr_akp_image = tf.image.resize(hr_akp_image, size=(8, 25), method=ResizeMethod.BICUBIC)
	if resize_lr:
		lr_akp_image = tf.image.resize(lr_akp_image, size=(64, 200), method=ResizeMethod.BICUBIC)
	hr_akp_image = tf.expand_dims(hr_akp_image, axis=0)
	lr_akp_image = tf.expand_dims(lr_akp_image, axis=0)
	akp_prediction = network.predict(lr_akp_image)
	Viewer().show_tensors([lr_akp_image[0], akp_prediction[0], hr_akp_image[0]], ["Input", "Prediction", "Ground Truth"])

def evaluate_qmq(network: tfk.Model, resize_lr: bool = False):
	loader = ImageLoader()
	hr_qmq_image = loader.load_image_as_tensor("{}/../bicubic/qmq-5140.png".format(current_dir))
	hr_qmq_image = tf.image.resize(hr_qmq_image, size=(64, 200), method=ResizeMethod.BICUBIC)
	lr_qmq_image = tf.image.resize(hr_qmq_image, size=(8, 25), method=ResizeMethod.BICUBIC)
	if resize_lr:
		lr_qmq_image = tf.image.resize(lr_qmq_image, size=(64, 200), method=ResizeMethod.BICUBIC)
	hr_qmq_image = tf.expand_dims(hr_qmq_image, axis=0)
	lr_qmq_image = tf.expand_dims(lr_qmq_image, axis=0)
	qmq_prediction = network.predict(lr_qmq_image)
	Viewer().show_tensors([lr_qmq_image[0], qmq_prediction[0], hr_qmq_image[0]], ["Input", "Prediction", "Ground Truth"])

def evaluate(network: tfk.Model, network_name: str, resize_lr: bool = False):
	checkpoint = "{}/training/checkpoints/{}/{}".format(current_dir, network_name, network_name)
	network.load_weights(checkpoint)
	# evaluate_plate(network, "bg-ACR-7528", resize_lr=resize_lr)
	# evaluate_plate(network, "bw-MRV-5646", resize_lr=resize_lr)
	# evaluate_plate(network, "gw-NKJ-3718", resize_lr=resize_lr)
	# evaluate_plate(network, "m-JIK2P60", resize_lr=resize_lr)
	# evaluate_plate(network, "m-THF9C95", resize_lr=resize_lr)
	# evaluate_plate(network, "r-NCI-1940", resize_lr=resize_lr)
	# evaluate_plate(network, "r-DAS-5954", resize_lr=resize_lr)
	# evaluate_plate(network, "rw-NGH-0835", resize_lr=resize_lr)
	# evaluate_plate(network, "wb-FVK-0456", resize_lr=resize_lr)
	# evaluate_plate(network, "wr-RQX-3349", resize_lr=resize_lr)
	# evaluate_akp(network, resize_lr=resize_lr)
	evaluate_qmq(network, resize_lr=resize_lr)

def main():
	training_files = glob("{}/../../data/training/high-resolution/*.jpg".format(current_dir))
	validation_files = glob("{}/../../data/validation/high-resolution/*.jpg".format(current_dir))

	low_quality_training_dataset = LowQualityDataset().create_batch_loader(files=training_files)
	low_quality_validation_dataset = LowQualityDataset().create_batch_loader(files=validation_files)

	# train(SRConvNet(), "srconvnet", training_dataset=low_quality_training_dataset, validation_dataset=low_quality_validation_dataset)
	# evaluate(SRConvNet(), "srconvnet", resize_lr=True)

	# train(SRResNet(), "srresnet", training_dataset=low_quality_training_dataset, validation_dataset=low_quality_validation_dataset)
	# evaluate(SRResNet(), "srresnet", resize_lr=True)

	low_resolution_training_dataset = LowResolutionDataset().create_batch_loader(files=training_files)
	low_resolution_validation_dataset = LowResolutionDataset().create_batch_loader(files=validation_files)

	# train(SRUpscaleResNet(), "srupscaleresnet_final", training_dataset=low_resolution_training_dataset, validation_dataset=low_resolution_validation_dataset)
	evaluate(SRUpscaleResNet(), "srupscaleresnet_final")

	# train(SRUpscaleConvNet(), "srupscaleconvnet_final", training_dataset=low_resolution_training_dataset, validation_dataset=low_resolution_validation_dataset)
	evaluate(SRUpscaleConvNet(), "srupscaleconvnet_final")

if __name__ == "__main__":
	main()
