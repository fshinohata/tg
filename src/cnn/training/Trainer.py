import shutil
import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow.keras.losses as tfkl
import tensorflow.keras.optimizers as tfko
import wandb

from cnn.logging.Logger import Logger
from cnn.loss.metrics import vgg_loss
from datetime import datetime
from os import path, makedirs
from wandb.keras import WandbCallback

current_dir = path.dirname(path.abspath(__file__))
checkpoints_dir = "{}/checkpoints".format(current_dir)

class Trainer:
	def __init__(self, name: str,
				network: tfk.Model,
				learning_rate: float = 0.004,
				optimizer: tfko.Optimizer = None,
				loss: tfkl.Loss = vgg_loss,
				logger: Logger = None) -> None:
		self.name = name
		self.network = network
		self.optimizer = tfko.Adam(learning_rate) if optimizer is None else optimizer
		self.loss = loss
		self.learning_rate = learning_rate
		self.logger = logger
		# self._setup_wandb()

	def compile_network(self) -> None:
		# if not self._load_network_model_if_exists():
		# 	self.network.compile(optimizer=self.optimizer, loss=self.loss, metrics=[tfkm.MeanSquaredError(), tfkm.RootMeanSquaredError(), tfkm.CosineSimilarity()])
		self._load_network_weights_if_exists()
		self.network.compile(optimizer=self.optimizer, loss=self.loss)

	def fit(self, training_dataset: tf.data.Dataset, validation_dataset: tf.data.Dataset = None, epochs: int = 50, steps_per_epoch: int = 300, iterations: int = 1) -> None:
		try:
			for i in range(iterations):
				history = self.network.fit(x=training_dataset, epochs=epochs, steps_per_epoch=steps_per_epoch, validation_data=validation_dataset, validation_steps=100)
				self._save_weights()
				# wandb.tensorflow.log(tf.summary.merge_all())
		except KeyboardInterrupt as e:
			print("\n\nTraining interrupted.\n\n")
			# self._save_weights()
			exit(0)

	def log_info(self, message: str) -> None:
		if self.logger is None:
			return
		self.logger.info(message)

	def _save_model(self) -> None:
		self._create_checkpoints_dir_if_not_exists()
		self._backup_existing_checkpoints()
		self.network.save("{}/{}".format(checkpoints_dir, self.name), save_format="tf")

	def _save_weights(self) -> None:
		self._create_checkpoint_dir_if_not_exists(self.name)
		self._backup_existing_checkpoints()
		self.network.save_weights("{}/{}/{}".format(checkpoints_dir, self.name, self.name), save_format="tf")

	def _load_network_weights_if_exists(self) -> None:
		checkpoint_name = "{}/{}/{}".format(checkpoints_dir, self.name, self.name)
		filename = "{}.index".format(checkpoint_name)
		if path.exists(filename):
			print("Loading network '{}' from '{}'...".format(self.name, filename))
			self.network.load_weights(checkpoint_name)
		else:
			print("Could not find checkpoint '{}'".format(filename))

	def _load_network_model_if_exists(self) -> bool:
		filename = "{}/{}".format(checkpoints_dir, self.name)
		if path.exists(filename):
			print("Loading network '{}' from '{}'...".format(self.name, filename))
			custom_objects = self.network.get_custom_objects()
			self.network = tfk.models.load_model(filename, custom_objects=custom_objects)
			return True
		return False

	def _create_checkpoints_dir_if_not_exists(self):
		makedirs("{}".format(checkpoints_dir), exist_ok=True)

	def _create_checkpoint_dir_if_not_exists(self, name: str) -> None:
		makedirs("{}/{}".format(checkpoints_dir, name), exist_ok=True)

	def _backup_existing_checkpoints(self):
		checkpoint_dir = "{}/{}".format(checkpoints_dir, self.name)
		now = datetime.now()
		if path.exists(checkpoint_dir):
			shutil.copytree(checkpoint_dir, "{}_{}_{}_{}_{}_{}_{}".format(checkpoint_dir, now.year, now.month, now.day, now.hour, now.minute, now.second))

	def _setup_wandb(self):
		wandb.login(key="00a906e8322d183d24c8d3cab76137fc9d076339")
		wandb.init(project="license-plate-super-resolution")
		config = wandb.config
		config.learning_rate = self.learning_rate
