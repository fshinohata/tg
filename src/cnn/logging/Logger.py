from datetime import datetime

class Logger:
	def __init__(self, path: str) -> None:
		self.path = path
		self._file = open(path, mode="a+")
		self.info("Logger up and running.")

	def info(self, message: str) -> None:
		self._log("INFO", message)

	def error(self, message: str) -> None:
		self._log("ERROR", message)

	def _log(self, level: str, message: str) -> None:
		self._file.write("[{}] [{}] {}\n".format(self._get_date(), level, message))

	def _get_date(self) -> str:
		return datetime.now().isoformat()
