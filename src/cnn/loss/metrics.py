import tensorflow as tf
import tensorflow.keras as tfk

from cnn.loss.vgg_extractor import extractor

def mse(ground_truth: tf.Tensor, prediction: tf.Tensor) -> float:
	loss = tf.reduce_mean(tfk.losses.mse(ground_truth, prediction))
	return loss

def mae(ground_truth: tf.Tensor, prediction: tf.Tensor) -> float:
	loss = tf.reduce_mean(tfk.losses.mae(ground_truth, prediction))
	return loss

def psnr(ground_truth: tf.Tensor, prediction: tf.Tensor) -> float:
	mse = tf.reduce_mean(tfk.losses.MSE(ground_truth, prediction))
	psnr = 10 * tf.math.log(1.0/mse)
	log10 = tf.math.log(tf.constant(10, dtype=psnr.dtype))
	return psnr/log10

def ssim(ground_truth: tf.Tensor, prediction: tf.Tensor) -> float:
	ssim = tf.image.ssim(ground_truth, prediction, max_val = 1.0)
	return tf.reduce_mean(ssim)

def vgg_loss(ground_truth: tf.Tensor, prediction: tf.Tensor) -> float:
	features_prediction = extractor(prediction)
	features_ground_truth = extractor(ground_truth)
	mae = tf.reduce_mean(tfk.losses.mae(ground_truth, prediction))
	return 0.1 * tf.reduce_mean(tfk.losses.mse(features_prediction, features_ground_truth)) + mae
