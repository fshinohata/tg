import tensorflow as tf

def _buildExtractor():
	vgg = tf.keras.applications.VGG19(include_top=False)
	config = vgg.get_layer('block2_conv2').get_config()
	config['activation'] = tf.keras.activations.linear
	config['name'] = 'output'
	output = tf.keras.layers.Conv2D(**config)(vgg.get_layer('block2_conv2').output)
	extractor = tf.keras.Model(inputs=vgg.input, outputs=output)
	extractor.layers[-1].set_weights(vgg.get_layer('block2_conv2').get_weights())
	return extractor

extractor = _buildExtractor()
