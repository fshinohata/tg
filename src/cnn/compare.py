import tensorflow as tf
import tensorflow.keras as tfk
import numpy as np

from cnn.dataset.LowQualityDataset import LowQualityDataset
from cnn.dataset.LowResolutionDataset import LowResolutionDataset
from cnn.io.ImageLoader import ImageLoader
from cnn.io.Viewer import Viewer
from cnn.logging.Logger import Logger
from cnn.loss.metrics import mae, mse, psnr, ssim, vgg_loss
from cnn.models.SRUpscaleConvNet import SRUpscaleConvNet
from cnn.models.SRUpscaleResNet import SRUpscaleResNet
from cnn.models.SRConvNet import SRConvNet
from cnn.models.SRResNet import SRResNet
from cnn.training.Trainer import Trainer
from datetime import datetime
from glob import glob
from tensorflow.python.ops.image_ops_impl import ResizeMethod
from os import path


current_dir = path.dirname(path.abspath(__file__))

def evaluate_image(network: tfk.Model, hr_image: tf.Tensor, resize_method: ResizeMethod):
	hr_shape = tf.shape(hr_image)
	lr_image = tf.image.resize(hr_image, size=(hr_shape[0] / 8, hr_shape[1] / 8), method=resize_method)

	hr_image = tf.expand_dims(hr_image, axis=0)
	lr_image = tf.expand_dims(lr_image, axis=0)

	lr_prediction = network.predict(lr_image)
	mae_loss = mae(hr_image, lr_prediction)
	mse_loss = mse(hr_image, lr_prediction)
	psnr_loss = psnr(hr_image, lr_prediction)
	ssim_loss = ssim(hr_image, lr_prediction)
	vgg_loss_loss = vgg_loss(hr_image, lr_prediction)

	return mae_loss, mse_loss, psnr_loss, ssim_loss, vgg_loss_loss

def evaluate_model(network: tfk.Model, files: list[str]) -> None:
	loader = ImageLoader()
	losses = {
		"mae": [],
		"mse": [],
		"psnr": [],
		"ssim": [],
		"vgg": [],
	}

	current_file = 1
	number_of_files = len(files)
	print("\n")

	for file in files:
		print("\rFile {} of {}...".format(current_file, number_of_files), end="")
		hr_image = loader.load_image_as_tensor(file)
		mae_loss, mse_loss, psnr_loss, ssim_loss, vgg_loss_loss = evaluate_image(network, hr_image, resize_method=ResizeMethod.BILINEAR)
		losses["mae"].append(mae_loss)
		losses["mse"].append(mse_loss)
		losses["psnr"].append(psnr_loss)
		losses["ssim"].append(ssim_loss)
		losses["vgg"].append(vgg_loss_loss)
		current_file += 1

	print("mae: {}".format(np.mean(losses["mae"])))
	print("mse: {}".format(np.mean(losses["mse"])))
	print("psnr: {}".format(np.mean(losses["psnr"])))
	print("ssim: {}".format(np.mean(losses["ssim"])))
	print("vgg: {}".format(np.mean(losses["vgg"])))

def load_network(network: tfk.Model, network_name: str) -> tfk.Model:
	checkpoint = "{}/training/checkpoints/{}/{}".format(current_dir, network_name, network_name)
	network.load_weights(checkpoint)
	return network

def main():
	validation_files = glob("{}/../../data/validation/high-resolution/*.jpg".format(current_dir))
	evaluate_model(load_network(SRUpscaleResNet(), "srupscaleresnet_final"), validation_files)
	evaluate_model(load_network(SRUpscaleConvNet(), "srupscaleconvnet_final"), validation_files)

if __name__ == "__main__":
	main()
