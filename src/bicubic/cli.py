import numpy as np
import tensorflow as tf

from os import sys
from tensorflow.python.ops.image_ops_impl import ResizeMethod

if len(sys.argv) < 4:
    print("Usage: python {} [src] [dst] [height,width]".format(sys.argv[0]))
    exit()

input_file = sys.argv[1]
output_file = sys.argv[2]
shape = sys.argv[3].split(",")

image = tf.io.read_file(input_file)
image = tf.image.decode_jpeg(image, channels=3)
image_shape = tf.shape(image)
resized_image = tf.image.resize(image, size=(int(shape[0]), int(shape[1])), method=ResizeMethod.BICUBIC)
resized_image = np.clip(resized_image, a_min=0, a_max=255)
resized_image = tf.image.encode_jpeg(resized_image)
tf.io.write_file(output_file, resized_image)
