from argparse import ArgumentParser
from dataset_generator.PlateSampleGenerator import PlateSampleGenerator, PlateTemplate
from dataset_generator.PlateImageGenerator import PlateImageGenerator


def parseArgs():
    parser = ArgumentParser(description="Brazilian Plate Generator")

    # Storage
    parser.add_argument("--dest", type=str, default="../../data", dest="dest")

    # Plate image generation
    parser.add_argument("--samples", type=int, default=1000, dest="samples")
    parser.add_argument("--hr-shape", type=int, nargs=2, default=[400, 130], dest="hrShape")
    parser.add_argument("--lr-shape", type=int, nargs=2, default=[50, 16], dest="lrShape")

    return parser.parse_args()


def generateSamples(dest: str, samples: int, hrShape: tuple, lrShape: tuple):
    sampleGeneratorReal = PlateSampleGenerator(PlateTemplate.REAL)
    sampleGeneratorMercosur = PlateSampleGenerator(PlateTemplate.MERCOSUR)
    sampleGeneratorWhiteBlack = PlateSampleGenerator(PlateTemplate.WHITE_BLACK)
    sampleGeneratorWhiteRed = PlateSampleGenerator(PlateTemplate.WHITE_RED)
    sampleGeneratorGreenWhite = PlateSampleGenerator(PlateTemplate.GREEN_WHITE)
    sampleGeneratorBlueWhite = PlateSampleGenerator(PlateTemplate.BLUE_WHITE)
    sampleGeneratorBlackGray = PlateSampleGenerator(PlateTemplate.BLACK_GRAY)
    sampleGeneratorRedWhite = PlateSampleGenerator(PlateTemplate.RED_WHITE)

    generator = PlateImageGenerator(destinationFolderPath=dest, hrShape=hrShape, lrShape=lrShape)

    print("Saving {} samples for each of the {} templates.".format(samples, 8))
    print("The generator may end up generating the same plate by chance.")
    print("In this case, the existing sample will just be overwritten, but the generator will consider it as a valid sample and count it.")
    print("This will result in up to {} images generated ({} HR, {} LR)".format(samples * 8 * 2, samples * 8, samples * 8))

    batchSize = 100
    samplesGenerated = 0

    while samples > 0:
        samplesToGenerate = samples if samples - batchSize < 0 else batchSize
        print("Saving samples ({} to {})...".format(samplesGenerated + 1, samplesGenerated + samplesToGenerate))

        generator.generate(sampleGeneratorReal, samplesToGenerate)
        generator.generate(sampleGeneratorMercosur, samplesToGenerate)
        generator.generate(sampleGeneratorWhiteBlack, samplesToGenerate)
        generator.generate(sampleGeneratorWhiteRed, samplesToGenerate)
        generator.generate(sampleGeneratorGreenWhite, samplesToGenerate)
        generator.generate(sampleGeneratorBlueWhite, samplesToGenerate)
        generator.generate(sampleGeneratorBlackGray, samplesToGenerate)
        generator.generate(sampleGeneratorRedWhite, samplesToGenerate)
        samples -= samplesToGenerate
        samplesGenerated += samplesToGenerate

    print("Done!")

def main():
    args = parseArgs()

    dest = args.dest
    samples = args.samples

    hrShape = tuple(args.hrShape)
    lrShape = tuple(args.lrShape)

    generateSamples(dest, samples, hrShape, lrShape)

if __name__ == "__main__":
    main()
