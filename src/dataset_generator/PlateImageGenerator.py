from PIL import Image, ImageDraw, ImageFont
import cv2
import os
import numpy as np
from typing import Tuple
from PlateSampleGenerator import PlateSampleGenerator
from PlateSample import PlateSample


class PlateImageGenerator:
    def __init__(self, destinationFolderPath: str, hrShape: Tuple[int, int], lrShape: Tuple[int, int]):
        # folders
        self.destinationFolderPath = destinationFolderPath
        self.lowResolutionFolder = '{}/low-resolution'.format(destinationFolderPath)
        self.highResolutionFolder = '{}/high-resolution'.format(destinationFolderPath)
        self.createDestinationFoldersIfNecessary()

        # parameters
        self.highResolution = hrShape
        self.lowResolution = lrShape
        self.interpolation = cv2.INTER_CUBIC

        # generators to use
        self.imageGenerators = []

    def createDestinationFoldersIfNecessary(self) -> None:
        os.makedirs(self.destinationFolderPath, exist_ok=True)
        os.makedirs(self.lowResolutionFolder, exist_ok=True)
        os.makedirs(self.highResolutionFolder, exist_ok=True)

    def generate(self, sampleGenerator: PlateSampleGenerator, numberOfSamples: int) -> None:
        template = cv2.imread(sampleGenerator.templatePath)
        font = ImageFont.truetype(font=sampleGenerator.fontPath, size=sampleGenerator.fontSize)
        fontColor = sampleGenerator.fontColor

        samples = sampleGenerator.generate(numberOfSamples)
        for sample in samples:
            # writes plate to template
            image = Image.fromarray(template)
            draw = ImageDraw.Draw(image)
            self.drawSample(draw, sample, font, fontColor)

            # returns the image to a numpy array
            image = np.array(image)
            image = cv2.resize(image, self.highResolution, interpolation=self.interpolation)

            # save high resolution and low resolution sample
            self.save(self.highResolutionFolder, self.prepend(sample.plate, sampleGenerator.prefix), image)
            self.generateLowResolutionCopy(image, self.prepend(sample.plate, sampleGenerator.prefix))

            for generate in self.imageGenerators:
                generate(image, self.prepend(sample.plate, sampleGenerator.prefix))

    def generateLowResolutionCopy(self, image, filename: str):
        # resize to low resolution to lose information,
        # resize back to the higher resolution (disabled) and save
        image = cv2.resize(image, self.lowResolution, interpolation=self.interpolation)
        # image = cv2.resize(image, self.highResolution, interpolation=self.interpolation)

        self.save(self.lowResolutionFolder, filename, image)

    def prepend(self, plate: str, prefix: str) -> str:
        return "{}-{}".format(prefix, plate)

    def save(self, folder: str, filename: str, image):
        cv2.imwrite('{}/{}.jpg'.format(folder, filename), image)

    def drawSample(self, interface: ImageDraw, sample: PlateSample, font: ImageFont.FreeTypeFont, fill: Tuple[int, int, int, int]) -> None:
        for i in range(len(sample.plate)):
            char = sample.plate[i]
            x, y = sample.charPositions[i]
            interface.text((x, y), char, font=font, fill=fill)
