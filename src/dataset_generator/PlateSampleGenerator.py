from dataset_generator.PlateSample import PlateSample
import numpy as np
import math
from enum import Enum
from typing import List, Tuple

templateFolder = './templates'
fontFolder = './fonts'


class PlateTemplate(Enum):
    REAL = 1
    MERCOSUR = 2
    WHITE_BLACK = 3
    WHITE_RED = 4
    GREEN_WHITE = 5
    BLUE_WHITE = 6
    BLACK_GRAY = 7
    RED_WHITE = 8


class PlateSampleGenerator(PlateSample):
    def __init__(self, template=PlateTemplate.REAL):
        if template == PlateTemplate.REAL:
            self.generate = self.generatorReal
            self.templatePath = '{}/real-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (30, 30, 30)
            self.prefix = "r"
        elif template == PlateTemplate.MERCOSUR:
            self.generate = self.generatorMercosur
            self.templatePath = '{}/mercosur-template.jpg'.format(templateFolder)
            self.fontPath = '{}/fe.ttf'.format(fontFolder)
            self.fontSize = 105
            self.fontColor = (40, 40, 40)
            self.prefix = "m"
        elif template == PlateTemplate.WHITE_BLACK:
            self.generate = self.generatorWhiteBlack
            self.templatePath = '{}/white-black-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (30, 30, 30)
            self.prefix = "wb"
        elif template == PlateTemplate.WHITE_RED:
            self.generate = self.generatorWhiteRed
            self.templatePath = '{}/white-red-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (20, 20, 224)
            self.prefix = "wr"
        elif template == PlateTemplate.GREEN_WHITE:
            self.generate = self.generatorGreenWhite
            self.templatePath = '{}/green-white-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (240, 240, 240)
            self.prefix = "gw"
        elif template == PlateTemplate.BLUE_WHITE:
            self.generate = self.generatorBlueWhite
            self.templatePath = '{}/blue-white-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (240, 240, 240)
            self.prefix = "bw"
        elif template == PlateTemplate.BLACK_GRAY:
            self.generate = self.generatorBlackGray
            self.templatePath = '{}/black-gray-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (200, 200, 200)
            self.prefix = "bg"
        elif template == PlateTemplate.RED_WHITE:
            self.generate = self.generatorRedWhite
            self.templatePath = '{}/red-white-template.jpg'.format(templateFolder)
            self.fontPath = '{}/mandator.ttf'.format(fontFolder)
            self.fontSize = 97
            self.fontColor = (240, 240, 240)
            self.prefix = "rw"

    def generatorReal(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionReal(plate[0], 0),
                self.getPositionReal(plate[1], 1),
                self.getPositionReal(plate[2], 2),
                self.getPositionReal(plate[3], 3),
                self.getPositionReal(plate[4], 4),
                self.getPositionReal(plate[5], 5),
                self.getPositionReal(plate[6], 6),
                self.getPositionReal(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionReal(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorMercosur(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 3)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 4)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(letters[3]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))

            charPositions = [
                self.getPositionMercosur(plate[0], 0),
                self.getPositionMercosur(plate[1], 1),
                self.getPositionMercosur(plate[2], 2),
                self.getPositionMercosur(plate[3], 3),
                self.getPositionMercosur(plate[4], 4),
                self.getPositionMercosur(plate[5], 5),
                self.getPositionMercosur(plate[6], 6),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionMercosur(self, char: str, index: int) -> Tuple[int, int]:
        offset = 60 + (68 * index)
        centering = (
            10 if char == '9' or char == 'W' else
            0
        )
        x = offset + centering
        y = 60
        return (x, y)

    def generatorWhiteBlack(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionWhiteBlack(plate[0], 0),
                self.getPositionWhiteBlack(plate[1], 1),
                self.getPositionWhiteBlack(plate[2], 2),
                self.getPositionWhiteBlack(plate[3], 3),
                self.getPositionWhiteBlack(plate[4], 4),
                self.getPositionWhiteBlack(plate[5], 5),
                self.getPositionWhiteBlack(plate[6], 6),
                self.getPositionWhiteBlack(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionWhiteBlack(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorWhiteRed(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionWhiteRed(plate[0], 0),
                self.getPositionWhiteRed(plate[1], 1),
                self.getPositionWhiteRed(plate[2], 2),
                self.getPositionWhiteRed(plate[3], 3),
                self.getPositionWhiteRed(plate[4], 4),
                self.getPositionWhiteRed(plate[5], 5),
                self.getPositionWhiteRed(plate[6], 6),
                self.getPositionWhiteRed(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionWhiteRed(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorGreenWhite(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionGreenWhite(plate[0], 0),
                self.getPositionGreenWhite(plate[1], 1),
                self.getPositionGreenWhite(plate[2], 2),
                self.getPositionGreenWhite(plate[3], 3),
                self.getPositionGreenWhite(plate[4], 4),
                self.getPositionGreenWhite(plate[5], 5),
                self.getPositionGreenWhite(plate[6], 6),
                self.getPositionGreenWhite(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionGreenWhite(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorBlueWhite(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionBlueWhite(plate[0], 0),
                self.getPositionBlueWhite(plate[1], 1),
                self.getPositionBlueWhite(plate[2], 2),
                self.getPositionBlueWhite(plate[3], 3),
                self.getPositionBlueWhite(plate[4], 4),
                self.getPositionBlueWhite(plate[5], 5),
                self.getPositionBlueWhite(plate[6], 6),
                self.getPositionBlueWhite(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionBlueWhite(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorBlackGray(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionBlackGray(plate[0], 0),
                self.getPositionBlackGray(plate[1], 1),
                self.getPositionBlackGray(plate[2], 2),
                self.getPositionBlackGray(plate[3], 3),
                self.getPositionBlackGray(plate[4], 4),
                self.getPositionBlackGray(plate[5], 5),
                self.getPositionBlackGray(plate[6], 6),
                self.getPositionBlackGray(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionBlackGray(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)

    def generatorRedWhite(self, numberOfSamples: int) -> List[PlateSample]:
        samples = []

        for i in range(numberOfSamples):
            # numbers: 48 - 57
            numbers = np.random.uniform(48, 58, 4)

            # letters: 65 - 90
            letters = np.random.uniform(65, 91, 3)

            plate = ''
            plate += chr(math.floor(letters[0]))
            plate += chr(math.floor(letters[1]))
            plate += chr(math.floor(letters[2]))
            plate += '-'
            plate += chr(math.floor(numbers[0]))
            plate += chr(math.floor(numbers[1]))
            plate += chr(math.floor(numbers[2]))
            plate += chr(math.floor(numbers[3]))

            charPositions = [
                self.getPositionRedWhite(plate[0], 0),
                self.getPositionRedWhite(plate[1], 1),
                self.getPositionRedWhite(plate[2], 2),
                self.getPositionRedWhite(plate[3], 3),
                self.getPositionRedWhite(plate[4], 4),
                self.getPositionRedWhite(plate[5], 5),
                self.getPositionRedWhite(plate[6], 6),
                self.getPositionRedWhite(plate[7], 7),
            ]

            samples.append(PlateSample(plate, charPositions))

        return samples

    def getPositionRedWhite(self, char: str, index: int) -> Tuple[int, int]:
        offset = 45 + (65 * index)
        centering = 16 if char == '1' or char == 'I' else 8 if char == '-' else 0
        x = offset + centering
        y = 50
        return (x, y)
