from typing import List, Tuple


class PlateSample:
    def __init__(self, plate: str, charPositions: List[Tuple[int, int]]):
        self.plate = plate
        self.charPositions = charPositions
