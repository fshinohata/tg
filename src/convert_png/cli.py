import numpy as np
import tensorflow as tf

from os import sys
from tensorflow.python.ops.image_ops_impl import ResizeMethod

if len(sys.argv) < 3:
    print("Usage: python {} [src] [dst]".format(sys.argv[0]))
    exit()

input_file = sys.argv[1]
output_file = sys.argv[2]

image = tf.io.read_file(input_file)
image = tf.image.decode_png(image, channels=3)
image = tf.image.encode_jpeg(image)
tf.io.write_file(output_file, image)
